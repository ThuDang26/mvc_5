<?php
class c_home {
    public function __construct()
    {
    }
    public function index(){
        include ("models/m_home.php");
        $m_home = new m_home();
        $homes = $m_home->read_product(0,10);
        $view = "views/v_home.php";
        include ("templates/layout.php");
    }
}
