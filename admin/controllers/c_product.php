<?php
include ("models/m_product.php");
include ("SimpleImage.php");
class c_product {
    public function addproduct() {
        $m_product = new m_product();
        if (isset($_POST["btn-submit"])) {
            $ID = NULL;
            $ID_DM = $_POST["ID_DM"];
            $CODE = $_POST["CODE"];
            $NAME = $_POST["ID"];
            $DESCRIPTION = $_POST["DESCRIPTION"];
            $PRICE = $_POST["PRICE"];
            $CREATED_DATE = $_POST["CREATED_DATE"];
            $QUANTITY = $_POST["QUANTITY"];
            $UNIT = $_POST["UNIT"];
            $STATUS = $_POST["STATUS"];
            //lấy được tên của hình ảnh
            $IMAGE = ($_FILES['IMAGE']['error'] == 0) ? $_FILES['IMAGE']['name'] : "";
            $result_insert = $m_product->insert_product($ID, $ID_DM, $CODE, $NAME, $DESCRIPTION, $IMAGE, $PRICE, $CREATED_DATE, $QUANTITY, $UNIT, $STATUS);
            if ($result_insert) {
                if ($IMAGE != ""){
                    //di chuyển hình ảnh vào thư mục source
                    move_uploaded_file($_FILES['IMAGE']['tmp_name'],"../public/layout/imageproduct/$IMAGE");
                    //resize lại kích cỡ ảnh //có cũng được ko có không sao
                    // $IMAGE = new SimpleImage();
                    // $dataImage['width'] = 1440;
                    // $dataImage['height'] = 585;
                    // $dataImage['path'] = "../public/layout/imagebanner";//đường dẫn thay đổi
                    // $dataImage['name'] = $hinh_tieu_de;
                    // $image->load($dataImage['name']. '/'. $dataImage['name']);
                    // $image->resize($dataImage['width'],$dataImage['height']);
                    // $image->save($dataImage['name']. '/'. $dataImage['name']);
                    //resize lại kích cỡ ảnh
                }
                echo "<script>alert('thêm thành công dữ liệu')</script>";
            } else {
                echo "<script>alert('thêm không thành công dữ liệu')</script>";
            }
        }
        $view = "views/product/v_addproduct.php";
        include ("templates/layout.php");
    }
}
?>