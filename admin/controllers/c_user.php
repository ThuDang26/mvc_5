<?php
@session_start();
include ("models/m_user.php");
class c_user{
    public function checkLogin(){
        $flag = false;
        if(isset($_POST['login'])){
            $username =  $_POST['NAME'];
            $password =  $_POST['PASSWORD'];
            // $m_user =  new m_user();
            $this->saveLoginToSession($username, $password);
            if (isset($_SESSION['user_admin'])){
                header("location:home.php");
            }else{
                $_SESSION['error_login']= "Name or Password incorrect!";
                header("location:login.php");
            }
            // echo $_POST['username'];
        }
    }

    public function logout(){
        unset($_SESSION['user_admin']);
        unset($_SESSION['error_login']);
        header("location:login.php");
    }

    public function saveLoginToSession($username, $password){
        $m_user = new m_user();
        $user = $m_user->read_user_by_id_pass($username, $password);
        if (!empty($user)){
            $_SESSION['user_admin'] = $user;
        }
    }
}
