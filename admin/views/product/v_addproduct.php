<div class="page-wrapper">
    <div class="container-fluid">
        <!-- ============================================================== -->
        <!-- Start Page Content -->
        <!-- ============================================================== -->
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <form class="form-horizontal" enctype="multipart/form-data" method="POST">
                        <div class="card-body">
                            <h4 class="card-title">Insert Product</h4>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >ID</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="ID" placeholder="ID">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Categories</label>
                                <div class="col-sm-9">
                                <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="ID_DM">
                                        <option>--Categories--</option>
                                        <option value="1">Jakets</option>
                                        <option value="2">Cost</option>
                                        <option value="3">Tops</option>
                                        <option value="4">Tops</option>
                                        <option value="5">T-shirts</option>
                                        <option value="6">Skirts</option>
                                        <option value="7">Bridals</option>
                                        <option value="8">Swetters</option>
                                </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Code</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="CODE" placeholder="Code">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Name</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="NAME" placeholder="Name">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Description</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="DESCRIPTION" placeholder="Description">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Image</label>
                                <div class="col-sm-9">
                                    <div class="custom-file">
                                        <input type="file" name="IMAGE" class="custom-file-input" id="validatedCustomFile" required>
                                        <label class="custom-file-label" for="validatedCustomFile">Choose file...</label>
                                        <div class="invalid-feedback">Example invalid custom file feedback</div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Price</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="PRICE" placeholder="Price">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Date</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="CREATED_DATE" placeholder="Date">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Quantity</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="QUANTITY" placeholder="Quantity">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="fname" class="col-sm-3 text-right control-label col-form-label" >Unit</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="fname" name="UNIT" placeholder="Unit">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="lname" class="col-sm-3 text-right control-label col-form-label">Status</label>
                                <div class="col-sm-9">
                                    <select class="select2 form-control custom-select" style="width: 100%; height:36px;" name="STATUS">
                                        <option>--Chọn--</option>
                                        <option value="1">Kích hoạt</option>
                                        <option value="0">Không kích hoạt</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="border-top">
                            <div class="card-body">
                                <button type="submit" name="btn-submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>