<div class="page-wrapper">
<?php include_once ("v_bread.php")?>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">List of Product</h5>
                                <div class="table-responsive">
                                    <table id="zero_config" class="table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>STT</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Code</th>
                                                <th>Desciption</th>
                                                <th>Image</th>
                                                <th>Price</th>
                                                <th>Created Date</th>
                                                <th>Quantity</th>
                                                <th>Unit</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <?php foreach ($homes as $key => $value) { ?>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $value->ID;?></td>
                                                <td><?php echo $value->NAME;?></td>
                                                <td><?php echo $value->ID_DM;?></td>
                                                <td><?php echo $value->CODE;?></td>
                                                <td><?php echo $value->DESCRIPTION;?></td>
                                                <td><?php echo $value->IMAGE;?></td>
                                                <td><?php echo $value->PRICE;?></td>
                                                <td><?php echo $value->CREATED_DATE;?></td>
                                                <td><?php echo $value->QUANTITY;?></td>
                                                <td><?php echo $value->UNIT;?></td>
                                                <td><?php echo $value->STATUS;?></td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="comment-footer"> 
                                    <button type="submit" name="edit" class="btn btn-cyan btn-sm">Edit</button>
                                    <!-- <button type="submit" name="insert" class="btn btn-success btn-sm">Insert</button> -->
                                   <a href="http://localhost:81/MVC_5/admin/addproduct.php" class="btn btn-success btn-sm" name="insert" type="submit">Insert</a>
                                    <button type="submit" name="delete" class="btn btn-danger btn-sm">Delete</button>
                                </div>
                                </table>
                                    <div class="row">
                                        <div class="col-sm-12 col-md-5">
                                            <div class="dataTables_info" id="zero_config_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                        </div>
                                        <div class="col-sm-12 col-md-7">
                                            <div class="dataTables_paginate paging_simple_numbers" id="zero_config_paginate">
                                                <ul class="pagination">
                                                    <li class="paginate_button page-item previous disabled" id="zero_config_previous"><a href="home.php" aria-controls="zero_config" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                                                    <li class="paginate_button page-item active"><a href="home.php" aria-controls="zero_config" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                                                    <li class="paginate_button page-item "><a href="#" aria-controls="zero_config" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                                    <li class="paginate_button page-item "><a href="#" aria-controls="zero_config" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                                    <li class="paginate_button page-item "><a href="#" aria-controls="zero_config" data-dt-idx="4" tabindex="0" class="page-link">4</a></li>
                                                    <li class="paginate_button page-item "><a href="#" aria-controls="zero_config" data-dt-idx="5" tabindex="0" class="page-link">5</a></li>
                                                    <li class="paginate_button page-item "><a href="#" aria-controls="zero_config" data-dt-idx="6" tabindex="0" class="page-link">6</a></li>
                                                    <li class="paginate_button page-item next" id="zero_config_next"><a href="#" aria-controls="zero_config" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End PAge Content -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Right sidebar -->
                <!-- ============================================================== -->
                <!-- .right-sidebar -->
                <!-- ============================================================== -->
                <!-- End Right sidebar -->
                <!-- ============================================================== -->
            </div>
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
            <!-- ============================================================== -->
            <!-- ============================================================== -->

            <!-- footer -->
            <!-- ============================================================== -->
            <footer class="footer text-center">
                All Rights Reserved by Threebirds-admin. Designed and Developed by <a href="https://wrappixel.com">Three Birds</a>.
            </footer>
            <!-- ============================================================== -->
            <!-- End footer -->
            <!-- ============================================================== -->
</div>


                                          