<?php
require_once ("database.php");
class m_user extends database
{
    // xây dựng phương thức kiểm tra tài khoản và mật khẩu
    public function read_user_by_id_pass($username, $password){
        $sql = "select * from admin where NAME = ? and PASSWORD = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($username, md5($password)));
    }

    /**
     * Đăng nhập tài khoản vào trang admin
     * @param string $username Tên đăng nhập
     * @param string $password Mật khẩu
     * @return object Thông tin entity ứng với quyền admin
     */
    // public function LoginAdmin($username, $password){
    //     $sql = "select * from user where UserName = ? and Password = ? and RoleID = 1";
    //     $this->setQuery($sql);
    //     return $this->loadRow(array($username, md5($password)));
    // }

    public function read_user_by_username($username){
        //xây dựng hàm đọc ng dùng theo user name
        $sql = "select * from admin where NAME = ?";
        $this->setQuery($sql);
        return $this->loadRow(array($username));
    }
}